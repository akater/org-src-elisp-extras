;;;  -*- lexical-binding: t -*-

(defconst-with-prefix fakemake
  feature 'org-src-elisp-extras
  authors "Dima Akater"
  first-publication-year-as-string "2023"
  org-files-in-order '("org-src-elisp-extras")
  site-lisp-config-prefix "40"
  license "GPL-3")
